<%-- 
    Document   : index.jsp
    Created on : 11-Feb-2019, 9:11:54 PM
    Author     : Hexiao Zhang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Self-Service Bank Web Site</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>
    <body>
        <h1>Self-Service Bank Web Site</h1>
        <p>
        Loan Calculator <br>
        Amount=Present Value; <br>
        Time=The length of loan in months;<br>
        Rate= the rate of interest on an annual basis; <br>
        Result:the monthly payment;<br> <br>
        Future Value of Saving <br>
        Amount=The amount put aside each month; <br>
        Time=the time that money will be put aside expressed in years;<br>
        Rate= the rate of annual interest; <br>  
        Result:Future value;<br> <br>        
        Saving Goal <br>
        Amount=The target amount desired; <br>
        Time=the length of time for savings express in years;<br>
        Rate= the rate of annual interest; <br>
        Result:The monthly amount to be saved;<br> 
        
        </p><br>

        <form action="CalculateMoney" method="post">

            <label class="pad_top2">Type of Calculator: </label>
            <select name = "type">
                <option value = "1">Loan Calculator</option>
                <option value = "2">Future Value Of Saving</option>
                <option value = "3">Saving Goal</option>
            </select><br>
    
            <label class="pad_top">Amount:</label>            
            <input type="text" name="amount" value="${user.amount}" required><br>

            <label class="pad_top">Time:</label>
            <input type="text" name="time" value="${user.time}" required><br>

            <label class="pad_top">Rate:</label>            
            <input type="text" name="rate" value="${user.rate}" required><br>           
            
            <label class="pad_top">Save the record</label>
            <form  class="save" name="record" value="${user.record}">
            <input type="radio" name="record" value="yes" checked>Yes
            <input type="radio" name="record" value="no">No
             
            <br>
            <label>&nbsp;</label>
            <input type="submit" value="submit" class="margin_left">
        </form>
    </body>
</html>