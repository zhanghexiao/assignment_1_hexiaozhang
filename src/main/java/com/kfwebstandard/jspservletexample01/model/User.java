package com.kfwebstandard.jspservletexample01.model;

import java.io.Serializable;

public class User implements Serializable {
     
    private double time;
    private double rate;
    private double amount;
    private int type;
    private double result;
    private String record;
    public User() {
          time = 0;
          type = 0;
          result = 0;
    }
    public User(double time, int type,double amount, double rate, String record) {
    this.time = time;
    this.type = type;
    this.amount = amount;
    this.rate = rate;
    this.record = record;
    }

    public double getTime() {
    return time;
    }

    public void setTime(double time) {
        this.time = time;
    }
    
    public int getType() {
    return type;
    }

    public void setTime(int type) {
        this.type = type;
    }
    
    public double getResult() {
    return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
    
    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getRate() {
    return rate;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
    return amount;
    }
    
    public void setRecord(String record) {
    this.record = record;
    }

    public String getRecord() {
    return record;
    }
    
    public String performcalculation() {
        switch (this.type) {
            case 1:
                loanCalculation();
                break;
            case 2:
                futureValueCalculation();
                break;
            case 3:
                savingsGoalCalculation();
                break;
        }
        return null;
    }
    
    private void loanCalculation() {
    
    double temp;
    temp = Math.pow(1+this.rate, -1*this.time);
    temp = this.amount*rate/(1-temp);
    this.result = Math.abs(temp);
    }
    
     private void futureValueCalculation() {
    double temp;
    temp = Math.pow(1+this.rate, this.time);
    temp = this.amount*(1-temp)/this.rate;
    this.result = Math.abs(temp);
    }
 
    private void savingsGoalCalculation() {
    double temp;
    temp = Math.pow(1+this.rate, this.time);
    temp = this.amount*this.rate/(1-temp);
    this.result = Math.abs(temp);
    }
    
    
    
}
